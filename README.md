# <od-modal>

> A set of modals to display information to the user

`<od-modal>` is a simple component that can be given custom content. Can either hijack the user experience or not.
`<od-modal-alert>` is a simple component that can be given custom content. It will hijack the user experience until the ok button is clicked.
`<od-modal-confirm>` is a simple component that can be given custom message content. It will hijack the user experience until the ok button is clicked and return the users decision.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-modal
```
## Usage
```
import '@orcden/od-modal';
```
```
<od-modal id="modal" scoped><p>Test</p></od-modal>
<od-modal-alert id="alert" scoped><p>Test 2</p></od-modal-alert>
<od-modal-confirm id="confirm" scoped><p>Test 3</p></od-modal-confirm>

document.querySelector( "#alert" ).alert( 'This is an alert' ); //displays the alert
document.querySelector( "#confirm" ).confirm( 'This is a confirm' ); //displays the confirm
```
    
## Attributes
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `scoped`  | Boolean  | false    | Can be set to tell the modal to scope itself to the next parent element with relative position (in CSS)  |
| `active`  | Boolean | false   | Controls CSS to show/hide the modal. |
| `hijack`  | Boolean | false   | Can be set to hijack the user experience until stated otherwise in JS. This is always true in alerts and confirms |

## Properties
### OD-Modal
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `scoped`  | Boolean  | false    | Can be set to tell the modal to scope itself to the next parent element with relative position (in CSS)  |
| `active`  | Boolean | false   | Controls CSS to show/hide the modal. |
| `hijack`  | Boolean | false   | Can be set to hijack the user experience until stated otherwise in JS. |

### OD-Modal-Alert & OD-Modal-Confirm
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `scoped`  | Boolean  | false    | Can be set to tell the modal to scope itself to the next parent element with relative position (in CSS)  |
| `active`  | Boolean | false   | Controls CSS to show/hide the modal. |
| `message`  | String | ''   | Can be used to set the displayed message of the modal |

## Functions
### OD-Modal
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `toggleModal`   | None | Toggles the modal on and off |

### OD-Modal-Alert
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `toggleAlert`   | None | Toggles the alert on and off |
| `alert`   | message | Async. Displays the alert and waits for the user input. Displays the given string to the user |

### OD-Modal-Confirm
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `toggleConfirm`   | None | Toggles the alert on and off |
| `confirm`   | message | Async. Displays the confirm and waits for the user input. Displays the given string to the user. Returns the Users input choice |

## Styling
- CSS variables are available to alter the default styling provided

| Shadow Parts     | Description           |
|------------------|-----------------------|
| modal           | The inner div used for the modal |
| content-box     | On alerts and confirms this is the div that holds the buttons and message |
| mesage          | On alerts and confirms this is the div that holds the text message |
| buttons-container | On alerts and confirms this is the div that holds the buttons |
| confirm-button | On alerts and confirms this is the OK button |
| confirm-button | On confirms this is the CANCEL button |

## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```
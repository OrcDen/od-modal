import { Selector, ClientFunction } from 'testcafe';
import { TestCafeComponentTestSuite } from '../../../node_modules/@orcden/od-component-test-suite';
import _config from './config';

const testSuite = new TestCafeComponentTestSuite( _config );
testSuite.runAllTests();

const model = testSuite.model;
const topButton = model.component;

fixture( 'OD-Top-Button Custom Tests' )
    .page( model.testServer );

test( 'Unit Tests - Properties - offset - behaviour', async t => {
    let selector = model.componentSelector;
    let offsetValue = await topButton.offset;

    await t.eval( () => { document.querySelector( selector ).offset = -1 }, {dependencies: { selector }} );
    
    await t
        .expect( topButton.offset ).eql( offsetValue, 'Property "active" - set -1 - does not update' );

    await t.eval( () => { document.querySelector( selector ).offset = 0 }, {dependencies: { selector }} );
    
    await t
        .expect( topButton.offset ).eql( 0, 'Property "active" - set 0 - returns 0' );

    await t.eval( () => { document.querySelector( selector ).offset = 1 }, {dependencies: { selector }} );
} );

test( 'Integration Tests - Properties - offset - behaviour', async t => {
    let selector = model.componentSelector;
    await t.eval( () => { document.querySelector( selector ).offset = 2 }, {dependencies: { selector }} );

    let offsetValue = await topButton.offset;

    await t
        .expect( topButton.withAttribute( 'offset', offsetValue.toString() ).exists ).ok( 'Attribute "offset" updates with property' );
        
    await t.eval( () => { document.querySelector( selector ).offset = 1 }, {dependencies: { selector }} );
} );

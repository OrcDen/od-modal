export default class OdModalBase extends HTMLElement {

    constructor( template ) {
        super();
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {        
        this._upgradeProperty( "active" );
        this._upgradeProperty( "scoped" );
    }

    disconnectedCallback() {}

    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        switch ( attrName ) {
            case "active": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setActiveAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setActiveAttribute( newValue );
                }
                break;
            }
            case "scoped": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setScopedAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setScopedAttribute( newValue );
                }
                break;
            }
        }
        if( this._updateAttributes ) {
            this._updateAttributes();
        }
    }

    get active() {
        return this.hasAttribute( "active" );
    }

    set active( isActive ) {
        if ( typeof isActive !== "boolean" ) {
            return;
        }
        this._setActiveAttribute( isActive );
    }

    _setActiveAttribute( newV ) {
        this._setBooleanAttribute( 'active', newV );
    }

    get scoped() {
        return this.hasAttribute( "scoped" );
    }

    set scoped( isScoped ) {
        if ( typeof isScoped !== "boolean" ) {
            return;
        }
        this._setScopedAttribute( isScoped );
    }

    _setScopedAttribute( newV ) {        
        this._setBooleanAttribute( 'scoped', newV );
    }

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }
}
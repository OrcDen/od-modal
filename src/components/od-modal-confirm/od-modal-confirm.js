import OdModalBase from '../../lib/odModalBase.js';
import '../od-modal';

const template = document.createElement( "template" );
template.innerHTML = `
     <style>

        #content-box {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        #message {
            margin-bottom: 1em;
        }

        #buttons-container {
            display: flex;
        }

        .button {
            display: flex;
            white-space: pre;
        }

    </style>

    <od-modal id='modal' hijack='true' exportparts='modal'>
        <div id='content-box' part='content-box'>
            <div id='message' part='message'></div>
            <slot></slot>
            <div id='buttons-container' part='buttons-container'>
                <button id='confirm-button' class='button' part='confirm-button'>Confirm</button>
                <button id='cancel-button' class='button' part='cancel-button'>Cancel</button>
            </div>
        </div>            
    </od-modal>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-modal-confirm" );

export class OdModalConfirm extends OdModalBase {

    constructor() {
        super( template );
    }

    connectedCallback() {     
        super.connectedCallback();
        this._message = '';
        
        let confirm = this.shadowRoot.querySelector( '#confirm-button' );
        confirm.addEventListener( "click", () => { this._dispatchConfirm() } );
        let cancel = this.shadowRoot.querySelector( '#cancel-button' );
        cancel.addEventListener( "click", () => { this._dispatchCancel() } );
    }

    disconnectedCallback() {
        super.disconnectedCallback();
    }    

    static get observedAttributes() {
        return ["active", "scoped"];
    }    
    
    get message() {
        return this._message;
    }

    set message( message ) {
        if ( typeof message !== "string" ) {
            return;
        }
        this._message = message;
        this.shadowRoot.querySelector( "#message" ).innerHTML = this.message;
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        super.attributeChangedCallback( attrName, oldValue, newValue );
    }

    _updateAttributes() {
        let modal = this.shadowRoot.querySelector( "#modal" );
        modal.active = this.active;
        modal.scoped = this.scoped;
    }

    _dispatchCancel() {
        this._dispatchModalEvent( false );
    }

    _dispatchConfirm() {
        this._dispatchModalEvent( true );
    }

    _dispatchModalEvent( confirmed ) {
        this.dispatchEvent(  new CustomEvent( 'od-modal-confirm', {
            bubbles: true,
            composed: true,
            'detail': {
                'confirmed': confirmed
            }
        } ) );
    }

    async confirm( message, callback ) {
        this.active = true;
        this.message = '';
        if( message ) {
            this.message = message;
        }
        return new Promise( (resolve) => {
            this.addEventListener( 'od-modal-confirm', ( e ) => { this._confirmCallback( e, resolve, callback ); } );
        });
    }

    _confirmCallback( e, resolve, callback ) {
        e.stopPropagation();
        if( e.detail.confirmed ) {
            if( callback !== undefined && callback() !== true ) {
                resolve( false );
            } else {
                this.active = false;
                this.removeEventListener( 'od-modal-confirm', ( e ) => { this._confirmCallback( e, resolve, callback ); } );
                resolve( true );
            }
        } else {
            this.active = false;
            this.removeEventListener( 'od-modal-confirm', ( e ) => { this._confirmCallback( e, resolve, callback ); } );
            resolve( false );                    
        } 
    }

    toggleConfirm( e ) {
        this.active = !this.active;
    }
}

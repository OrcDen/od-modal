import OdModalBase from '../../lib/odModalBase.js';
import '../od-modal';

const template = document.createElement( "template" );
template.innerHTML = `
     <style>

        #content-box {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        #message {
            margin-bottom: 1em;
        }

        #buttons-container {
            display: flex;
        }

        .button {
            display: flex;
            white-space: pre;
        }

    </style>

    <od-modal id='modal' hijack='true' exportparts='modal'>
        <div id='content-box' part='content-box'>
            <div id='message' part='message'></div>
            <slot></slot>
            <div id='buttons-container' part='buttons-container'>
                <button id='confirm-button' class='button' part='confirm-button'>OK</button>
            </div>
        </div>            
    </od-modal>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-modal-alert" );

export class OdModalAlert extends OdModalBase {

    constructor() {
        super( template );
    }

    connectedCallback() {     
        super.connectedCallback();
        this._message = '';
        
        let confirm = this.shadowRoot.querySelector( '#confirm-button' );
        confirm.addEventListener( "click", () => { this._dispatchConfirm() } );
    }

    disconnectedCallback() {
        super.disconnectedCallback();
    }    

    static get observedAttributes() {
        return ["active", "scoped"];
    }    
    
    get message() {
        return this._message;
    }

    set message( message ) {
        if ( typeof message !== "string" ) {
            return;
        }
        this._message = message;
        this.shadowRoot.querySelector( "#message" ).innerHTML = this.message;
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        super.attributeChangedCallback( attrName, oldValue, newValue );
    }

    _updateAttributes() {
        let modal = this.shadowRoot.querySelector( "#modal" );
        modal.active = this.active;
        modal.scoped = this.scoped;
    }

    _dispatchConfirm() {
        this.active = false;
        this.dispatchEvent(  new CustomEvent( 'od-modal-confirm', {
            bubbles: true,
            composed: true,
            'detail': {
                'confirmed': true
            }
        } ) );
    }

    async alert( message ) {
        this.active = true;
        this.message = '';
        if( message ) {
            this.message = message;
        }
        return new Promise( (resolve) => {
            this.addEventListener( 'od-modal-confirm', 
                                    ( e ) => { 
                                        e.stopPropagation();
                                        resolve( e.detail.confirmed ); },
                                    {once:true} );
        });
    }

    toggleAlert() {
        this.active = !this.active;
    }
}

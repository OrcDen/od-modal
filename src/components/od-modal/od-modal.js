import OdModalBase from '../../lib/odModalBase.js'

const template = document.createElement( "template" );
template.innerHTML = `
     <style>

        :host( :not( [active] ) ) #od-modal {
            display: none;
        }

        :host( :not( [scoped] ) ) #od-modal {
            position: fixed;
        }

        :host( [scoped] ) #od-modal {
            position: absolute;
        }

        #content-slot::slotted(*) {
            -webkit-box-shadow: 3px 6px 7px 0px rgba(0,0,0,0.5);
            -moz-box-shadow: 3px 6px 7px 0px rgba(0,0,0,0.5);
            box-shadow: 3px 6px 7px 0px rgba(0,0,0,0.5);
        }

        #od-modal {
            display: flex;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background-color: rgba(191, 191, 191, 0.35);
            z-index: 9000;
            align-items: center;
            justify-content: center;
            padding: 5em;
        }

        @media print {
            #od-modal {
                position: static !important;
            }
        }

    </style>

    <div id='od-modal' part='modal'>
        <slot id='content-slot'></slot>
    </div>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "od-modal" );

export class OdModal extends OdModalBase {

    constructor() {
        super( template );
    }

    connectedCallback() {     
        super.connectedCallback();
        this._upgradeProperty( "hijack" );

        let modal = this.shadowRoot.querySelector( '#od-modal' );
        modal.addEventListener( 'click', ( e ) => { this.toggleModal( e ) } );
    }

    disconnectedCallback() {
        super.disconnectedCallback();
    }    

    static get observedAttributes() {
        return ["active", "scoped", "hijack"];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        super.attributeChangedCallback( attrName, oldValue, newValue );
        switch( attrName ) {
            case "hijack": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setHijackAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setHijackAttribute( newValue );
                }
                break;
            }
        }
    }
    
    get hijack() {
        return this.hasAttribute( "hijack" );
    }

    set hijack( hijack ) {
        if ( typeof hijack !== "boolean" ) {
            return;
        }
        this._setHijackAttribute( hijack );
    }

    _setHijackAttribute( newV ) {
        this._setBooleanAttribute( 'hijack', newV );
    }

    toggleModal( e ) {
        if ( this.hijack || e && e.target.id !== 'od-modal' ) {
            return;
        }
        this.active = !this.active;
    }
}
